const helper = require("../../../__fixtures/test-helper").create();

describe(helper.testName(__filename, 4), function() {
  it("tests the getConstructorArgumentNames", () => {
    let utils = require("red-bed").Common.Utils.create();
    var constructorArgs = utils.getConstructorArgumentNamesFromClass(
      require("red-bed").Common.Utils
    );
    expect(constructorArgs).to.eql(["config"]);
  });
});
