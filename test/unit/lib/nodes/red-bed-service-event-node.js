const testhelper = require("../../../__fixtures/test-helper").create();
const helper = require("node-red-node-test-helper");
const NODE_NAME = "red-bed-service-event";
const nodeInstance = require(`../../../../lib/nodes/${NODE_NAME}-node.js`);

helper.init(require.resolve("node-red"));

describe(testhelper.testName(__filename, 4), function() {
  let eventService = require(`../../../../lib/services/red-bed-event/red-bed-event-service.js`).create(
    testhelper.mockConfig(),
    testhelper.mockCommon()
  );
  beforeEach(function(done) {
    require("red-bed").Container.instance = () => {
      return {
        services: {
          "red-bed-event": eventService
        }
      };
    };
    helper.startServer(done);
  });

  afterEach(function(done) {
    helper.unload();
    helper.stopServer(done);
    delete require.cache[require.resolve("red-bed")];
  });

  it("should be loaded", function(done) {
    var flow = [
      { id: "n1", type: `service-event-rb`, name: `${NODE_NAME}-node` }
    ];
    helper.load(nodeInstance, flow, function(e) {
      if (e) return done(e);
      var n1 = helper.getNode("n1");
      n1.should.have.property("name", `${NODE_NAME}-node`);
      done();
    });
  });

  it("should subscribe to message with defaults", function(done) {
    var flow = [
      {
        id: "n1",
        type: `service-event-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        service: "red-bed-event",
        eventOperation: "ON",
        eventKey: "*"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      n2.on("input", function(msg) {
        expect(msg.payload).to.eql({ test: "payload" });
        done();
      });
      eventService.emit("test-event", { test: "payload" });
    });
  });

  it("should emit a message", function(done) {
    var flow = [
      {
        id: "n1",
        type: `service-event-rb`,
        name: `${NODE_NAME}-node`,
        wires: [[]],
        service: "red-bed-event",
        operation: "EMIT",
        eventPayloadPath: "msg.payload",
        eventKey: "test-event"
      },
      {
        id: "n2",
        type: `service-event-rb`,
        name: `${NODE_NAME}-node-2`,
        wires: [["n3"]],
        service: "red-bed-event",
        eventOperation: "ON",
        eventKey: "*"
      },
      { id: "n3", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      var n3 = helper.getNode("n3");
      n3.on("input", function(msg) {
        expect(msg.payload.test).to.eql("payload");
        done();
      });
      n1.receive({ payload: { test: "payload" } });
    });
  });

  it("should fail, eventPayloadPath not configured", function(done) {
    var flow = [
      {
        id: "n1",
        type: `service-event-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        service: "red-bed-event",
        operation: "EMIT",
        eventKey: "*"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal("eventPayloadPath not configured");
        done();
      });
      n1.receive({ payload: { test: "payload" } });
    });
  });

  it("should fail, eventKey not configured", function(done) {
    var flow = [
      {
        id: "n1",
        type: `service-event-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        service: "red-bed-event",
        operation: "EMIT",
        eventPayloadPath: "msg.payload"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal("eventKey not configured");
        done();
      });
      n1.receive({ payload: { test: "payload" } });
    });
  });
});
