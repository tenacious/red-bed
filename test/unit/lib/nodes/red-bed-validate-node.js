const testhelper = require("../../../__fixtures/test-helper").create();
const helper = require("node-red-node-test-helper");
const NODE_NAME = "red-bed-validate";
const nodeInstance = require(`../../../../lib/nodes/${NODE_NAME}-node.js`);

helper.init(require.resolve("node-red"));

describe(testhelper.testName(__filename, 4), function() {
  let validateService = require(`../../../../lib/services/red-bed-validate/red-bed-validate-service.js`).create(
    testhelper.mockConfig(),
    testhelper.mockCommon()
  );
  beforeEach(function(done) {
    require("red-bed").Container.instance = () => {
      return {
        services: {
          "red-bed-validate": validateService
        }
      };
    };
    helper.startServer(done);
  });

  afterEach(function(done) {
    helper.unload();
    helper.stopServer(done);
    delete require.cache[require.resolve("red-bed")];
  });

  it("should be loaded", function(done) {
    var flow = [{ id: "n1", type: `validate-rb`, name: `${NODE_NAME}-node` }];
    helper.load(nodeInstance, flow, function(e) {
      if (e) return done(e);
      var n1 = helper.getNode("n1");
      n1.should.have.property("name", `${NODE_NAME}-node`);
      done();
    });
  });

  it("should do a standard validate true", function(done) {
    var flow = [
      {
        id: "n1",
        type: `validate-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        validatePayloadPath: "msg.payload.toBeValidateed",
        validate: {
          type: "object",
          properties: {
            foo: { type: "integer" },
            bar: { type: "string" }
          },
          required: ["foo", "bar"]
        }
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        expect(msg.payload.validated.valid).to.be.true;
        done();
      });
      n1.receive({ payload: { toBeValidateed: { foo: 1, bar: "test" } } });
    });
  });

  it("should do a standard validate false", function(done) {
    var flow = [
      {
        id: "n1",
        type: `validate-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        validatePayloadPath: "msg.payload.toBeValidateed",
        validate: {
          type: "object",
          properties: {
            foo: { type: "integer" },
            bar: { type: "string" }
          },
          required: ["foo", "bar"]
        }
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        expect(msg.payload.validated.valid).to.be.false;
        done();
      });
      n1.receive({ payload: { toBeValidateed: { foo: 1, blah: "test" } } });
    });
  });

  it("should do a object validate, not found", function(done) {
    var flow = [
      {
        id: "n1",
        type: `validate-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        validatePayloadPath: "msg.payload.blah",
        validate: {
          type: "object",
          properties: {
            foo: { type: "integer" },
            bar: { type: "string" }
          },
          required: ["foo", "bar"]
        }
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(error) {
        expect(error.message).to.equal(
          "unable to find validation payload on path: msg.payload.blah"
        );
        done();
      });
      n1.receive({ payload: { test: { data: 2 } } });
    });
  });

  it("should fail, unconfigured validate", function(done) {
    var flow = [
      {
        id: "n1",
        type: `validate-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        validatePayloadPath: "msg.payload"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal("validate not configured");
        done();
      });
      n1.receive({ payload: { test: { data: 1 } } });
    });
  });
});
