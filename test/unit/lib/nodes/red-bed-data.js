const testhelper = require("../../../__fixtures/test-helper").create();
const NODE_NAME = "red-bed-kv-store";
const nodeInstance = require(`../../../../lib/nodes/${NODE_NAME}-node.js`);
const datasvc = require(`../../../../lib/services/red-bed-data/red-bed-data-service.js`);

const { Container } = require("red-bed");

helper.init(require.resolve("node-red"));

describe(testhelper.testName(__filename, 4), function() {
  this.timeout(10000);

  before(function() {
    this.svc = datasvc.create(testhelper.mockConfig(), testhelper.mockCommon());
  });

  beforeEach(function(done) {
    sinon.stub(Container, "instance").returns({
      services: {
        "red-bed-data": this.svc
      }
    });
    helper.startServer(done);
  });

  afterEach(function(done) {
    helper.unload();
    helper.stopServer(done);
    sinon.restore();
  });

  it("should be loaded", function(done) {
    var flow = [
      {
        id: "n1",
        type: `key-value-rb`,
        name: `${NODE_NAME}-node`
      }
    ];
    helper.load(nodeInstance, flow, function(e) {
      if (e) return done(e);
      const n1 = helper.getNode("n1");
      n1.should.have.property("name", `${NODE_NAME}-node`);
      done();
    });
  });

  it("should throw an error: invalid operation", async () => {
    var flow = [
      {
        id: "n1",
        type: `key-value-rb`,
        wires: [["n2"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        kvOperation: "INVALID"
      },
      {
        id: "n2",
        type: "helper"
      }
    ];

    const handleAssert = await loadAsAsync(nodeInstance, flow);

    const n1 = helper.getNode("n1");
    n1.receive({
      payload: {
        ourTestingValue: "testingTheInsert"
      }
    });

    await handleAssert(msg => {
      expect(msg.error.message).to.have.string("Invalid operation; ");
    });
  });

  it("should throw an error: no payload path", async () => {
    var flow = [
      {
        id: "n1",
        type: `key-value-rb`,
        wires: [["n2"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        kvOperation: "SET"
      },
      {
        id: "n2",
        type: "helper"
      }
    ];

    const handleAssert = await loadAsAsync(nodeInstance, flow);

    const n1 = helper.getNode("n1");
    n1.receive({
      payload: {
        ourTestingValue: "testingTheInsert"
      }
    });

    await handleAssert(msg => {
      expect(msg.error.message).to.have.string(
        "valuePayloadPath not configured"
      );
    });
  });

  it("should insert a document", async () => {
    var flow = [
      {
        id: "n1",
        type: `key-value-rb`,
        wires: [["n2"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        valuePayloadPath: "msg.payload.ourTestingValue",
        kvOperation: "SET"
      },
      {
        id: "n2",
        type: "helper"
      }
    ];

    const handleAssert = await loadAsAsync(nodeInstance, flow);

    const n2 = helper.getNode("n2");
    const n1 = helper.getNode("n1");
    n2.on("input", function(msg) {
      expect(msg.payload.kv_result).to.have.property("key", "ourTestingKey");
      expect(msg.payload.kv_result).to.have.property(
        "value",
        "testingTheInsert"
      );
    });

    n1.receive({
      payload: {
        ourTestingValue: "testingTheInsert"
      }
    });

    await handleAssert();
  });

  it("should retrieve a document", async () => {
    var flow = [
      {
        id: "n1",
        type: `key-value-rb`,
        wires: [["n2"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        kvOperation: "GET"
      },
      {
        id: "n2",
        type: "helper"
      }
    ];

    const handleAssert = await loadAsAsync(nodeInstance, flow);

    const n2 = helper.getNode("n2");
    const n1 = helper.getNode("n1");
    n2.on("input", function(msg) {
      expect(msg.payload.kv_result).to.have.property("key", "ourTestingKey");
      expect(msg.payload.kv_result).to.have.property(
        "value",
        "testingTheInsert"
      );
    });

    n1.receive({
      payload: {}
    });

    await handleAssert();
  });

  it("should update a document", async () => {
    var flow = [
      {
        id: "n1",
        type: `key-value-rb`,
        wires: [["n2"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        valuePayloadPath: "msg.payload.ourTestingValue",
        kvOperation: "SET"
      },
      {
        id: "n2",
        type: "helper"
      },
      {
        id: "n3",
        type: `key-value-rb`,
        wires: [["n4"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        kvOperation: "GET"
      },
      {
        id: "n4",
        type: "helper"
      }
    ];

    const handleAssert = await loadAsAsync(nodeInstance, flow);

    const n2 = helper.getNode("n2");
    const n1 = helper.getNode("n1");
    const n4 = helper.getNode("n4");
    const n3 = helper.getNode("n3");

    n4.on("input", function(msg) {
      expect(msg.payload.kv_result).to.have.property("key", "ourTestingKey");
      expect(msg.payload.kv_result).to.have.property(
        "value",
        "testingTheUpdate"
      );
    });

    n2.on("input", function(msg) {
      expect(msg.payload.kv_result).to.have.property("key", "ourTestingKey");
      expect(msg.payload.kv_result).to.have.property(
        "value",
        "testingTheUpdate"
      );

      n3.receive({
        payload: {}
      });
    });

    n1.receive({
      payload: {
        ourTestingValue: "testingTheUpdate"
      }
    });

    await handleAssert();
  });

  it("should remove a document", async () => {
    var flow = [
      {
        id: "n1",
        type: `key-value-rb`,
        wires: [["n2"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        valuePayloadPath: "msg.payload.ourTestingValue",
        kvOperation: "UNSET"
      },
      {
        id: "n2",
        type: "helper"
      },
      {
        id: "n3",
        type: `key-value-rb`,
        wires: [["n4"]],
        name: `${NODE_NAME}-node`,
        key: "ourTestingKey",
        kvOperation: "GET"
      },
      {
        id: "n4",
        type: "helper"
      }
    ];

    const handleAssert = await loadAsAsync(nodeInstance, flow);

    const n2 = helper.getNode("n2");
    const n1 = helper.getNode("n1");
    const n4 = helper.getNode("n4");
    const n3 = helper.getNode("n3");

    n4.on("input", function(msg) {
      expect(msg.payload.kv_result).to.be.null;
    });

    n2.on("input", function(msg) {
      expect(msg.payload.kv_result).to.equal(1);

      n3.receive({
        payload: {}
      });
    });

    n1.receive({
      payload: {
        ourTestingValue: "testingTheUpdate"
      }
    });

    await handleAssert();
  });
});
