const testhelper = require("../../../__fixtures/test-helper").create();
const helper = require("node-red-node-test-helper");
const nodeInstance = require(`../../../../lib/nodes/red-bed-state-machine-node.js`);
const { Container } = require("red-bed");

helper.init(require.resolve("node-red"));

describe(testhelper.testName(__filename, 4), function() {
  let ERROR = null;

  beforeEach(function(done) {
    this.timeout(4000);
    sinon.stub(Container, "instance").returns({
      services: {
        "red-bed-statemachine": {
          getState: () => {
            if (ERROR) throw new Error(ERROR);
            return "test succeed";
          },
          setState: () => {
            if (ERROR) throw new Error(ERROR);
            return "test succeed";
          }
        }
      }
    });
    helper.startServer(done);
  });

  afterEach(function(done) {
    if (ERROR) ERROR = null;
    helper.unload();
    helper.stopServer(done);
    sinon.restore();
  });

  it("should be loaded", function(done) {
    var flow = [{ id: "n1", type: `state-rb`, name: `state-rb` }];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.should.have.property("name", `state-rb`);
      done();
    });
  });

  it("should process incoming message - GET", function(done) {
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "GET"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        msg.redbed.lastState.should.equal("test succeed");
        done();
      });
      n1.receive({});
    });
  });

  it("should process incoming message error - GET", function(done) {
    ERROR = "test error";
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "GET"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal(ERROR);

        done();
      });
      n1.receive({});
    });
  });

  it("should process incoming message - SET", function(done) {
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "SET"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        msg.redbed.lastState.should.equal("test succeed");
        done();
      });
      n1.receive({});
    });
  });

  it("should process incoming message error - SET", function(done) {
    ERROR = "test error";
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "SET"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal(ERROR);

        done();
      });
      n1.receive({});
    });
  });

  it("should process incoming message - !EXPECT", function(done) {
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "EXPECT",
        stateValue: "!test failed"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        msg.redbed.lastState.should.equal("test succeed");
        done();
      });
      n1.receive({});
    });
  });

  it("should process incoming message -  EXPECT", function(done) {
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "EXPECT",
        stateValue: "test succeed"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        msg.redbed.lastState.should.equal("test succeed");
        done();
      });
      n1.receive({});
    });
  });

  it("should not process incoming message - !EXPECT", function(done) {
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "EXPECT",
        stateValue: "!test succeed"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      let outerMsg = null;
      n2.on("input", function(msg) {
        outerMsg = msg;
      });
      setTimeout(function() {
        if (!outerMsg) done();
      }, 1000);
      n1.receive({});
    });
  });

  it("should not process incoming message -  EXPECT", function(done) {
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "EXPECT",
        stateValue: "test failed"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      let outerMsg = null;
      n2.on("input", function(msg) {
        outerMsg = msg;
      });
      setTimeout(function() {
        if (!outerMsg) done();
      }, 1000);
      n1.receive({});
    });
  });

  it("should process incoming message error - EXPECT", function(done) {
    ERROR = "test error";
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "EXPECT"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal(ERROR);

        done();
      });
      n1.receive({});
    });
  });

  it("should process incoming message error - EXPECT the unexpected", function(done) {
    ERROR = "test error";
    var flow = [
      {
        id: "n1",
        type: `state-rb`,
        name: `state-rb`,
        wires: [["n2"]],
        stateOperation: "unexpected"
      },
      { id: "n2", type: "helper" }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal("unexpected state operation: unexpected");

        done();
      });
      n1.receive({});
    });
  });
});
