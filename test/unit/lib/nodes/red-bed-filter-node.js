const testhelper = require("../../../__fixtures/test-helper").create();
const helper = require("node-red-node-test-helper");
const NODE_NAME = "red-bed-filter";
const nodeInstance = require(`../../../../lib/nodes/${NODE_NAME}-node.js`);

helper.init(require.resolve("node-red"));

describe(testhelper.testName(__filename, 4), function() {
  let filterService = require(`../../../../lib/services/red-bed-filter/red-bed-filter-service.js`).create(
    testhelper.mockConfig(),
    testhelper.mockCommon()
  );
  beforeEach(function(done) {
    require("red-bed").Container.instance = () => {
      return {
        services: {
          "red-bed-filter": filterService
        }
      };
    };
    helper.startServer(done);
  });

  afterEach(function(done) {
    helper.unload();
    helper.stopServer(done);
    delete require.cache[require.resolve("red-bed")];
  });

  it("should be loaded", function(done) {
    var flow = [
      {
        id: "n1",
        type: `filter-rb`,
        name: `${NODE_NAME}-node`
      }
    ];
    helper.load(nodeInstance, flow, function(e) {
      if (e) return done(e);
      var n1 = helper.getNode("n1");
      n1.should.have.property("name", `${NODE_NAME}-node`);
      done();
    });
  });

  it("should do a standard filter", function(done) {
    var flow = [
      {
        id: "n1",
        type: `filter-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        filterPayloadPath: "msg.payload.toBeFiltered",
        filter: {
          $in: ["hello", "world"]
        }
      },
      {
        id: "n2",
        type: "helper"
      }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        expect(msg.payload.filtered).to.eql(["hello"]);
        done();
      });
      n1.receive({
        payload: {
          toBeFiltered: ["hello", "sifted", "array!"]
        }
      });
    });
  });

  it("should do a object filter, found", function(done) {
    var flow = [
      {
        id: "n1",
        type: `filter-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        filterPayloadPath: "msg.payload",
        filter: {
          "test.data": {
            $eq: 1
          }
        }
      },
      {
        id: "n2",
        type: "helper"
      }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        expect(msg.payload.filtered).to.eql([
          {
            test: {
              data: 1
            }
          }
        ]);
        done();
      });
      n1.receive({
        payload: {
          test: {
            data: 1
          }
        }
      });
    });
  });

  it("should do a object filter, not found", function(done) {
    var flow = [
      {
        id: "n1",
        type: `filter-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        filterPayloadPath: "msg.payload",
        filter: {
          "test.data": {
            $eq: 1
          }
        }
      },
      {
        id: "n2",
        type: "helper"
      }
    ];
    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        expect(msg.payload.filtered).to.eql([]);
        done();
      });
      n1.receive({
        payload: {
          test: {
            data: 2
          }
        }
      });
    });
  });

  it("should do an object filter, match array in object", function(done) {
    var flow = [
      {
        id: "n1",
        type: `filter-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        filterPayloadPath: "msg.payload",
        filter: {
          "test.arr.0.test": {
            $eq: 2
          }
        }
      },
      {
        id: "n2",
        type: "helper"
      }
    ];

    helper.load(nodeInstance, flow, function() {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function(msg) {
        expect(msg.payload.filtered).to.eql([
          {
            test: {
              data: 1,
              arr: [
                {
                  test: 2
                }
              ]
            }
          }
        ]);
        done();
      });
      n1.receive({
        payload: {
          test: {
            data: 1,
            arr: [
              {
                test: 2
              }
            ]
          }
        }
      });
    });
  });

  it("should fail, unconfigured filterPayloadPath", function(done) {
    var flow = [
      {
        id: "n1",
        type: `filter-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        filter: {
          "test.data": {
            $eq: 1
          }
        }
      },
      {
        id: "n2",
        type: "helper"
      }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal("filterPayloadPath not configured");
        done();
      });
      n1.receive({
        payload: {
          test: {
            data: 1
          }
        }
      });
    });
  });

  it("should fail, unconfigured filter", function(done) {
    var flow = [
      {
        id: "n1",
        type: `filter-rb`,
        name: `${NODE_NAME}-node`,
        wires: [["n2"]],
        filterPayloadPath: "msg.payload"
      },
      {
        id: "n2",
        type: "helper"
      }
    ];
    helper.load(nodeInstance, flow, function() {
      var n1 = helper.getNode("n1");
      n1.on("node-error", function(e) {
        e.message.should.equal("filter not configured");
        done();
      });
      n1.receive({
        payload: {
          test: {
            data: 1
          }
        }
      });
    });
  });
});
