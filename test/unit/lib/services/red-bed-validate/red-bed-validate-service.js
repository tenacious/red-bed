const testhelper = require("../../../../__fixtures/test-helper").create();

describe(testhelper.testName(__filename, 4), function() {
  it("should validate under a range of conditions", function() {
    let validateService = require(`../../../../../lib/services/red-bed-validate/red-bed-validate-service.js`).create(
      testhelper.mockConfig(),
      testhelper.mockCommon()
    );

    let schema = {
      type: "object",
      properties: {
        foo: { type: "integer" },
        bar: { type: "string" }
      },
      required: ["foo", "bar"]
    };

    expect(validateService.validate({ foo: 1, bar: "test" }, schema)).to.eql({
      valid: true,
      errors: null
    });

    expect(Object.keys(validateService.compiledSchemas).length).to.equal(1);

    let result = validateService.validate({ foo: 1, blah: "test" }, schema);

    expect(result).to.eql({
      valid: false,
      errors: [
        {
          keyword: "required",
          dataPath: "",
          schemaPath: "#/required",
          params: {
            missingProperty: "bar"
          },
          message: "should have required property 'bar'"
        }
      ]
    });

    expect(Object.keys(validateService.compiledSchemas).length).to.equal(1);

    expect(() =>
      validateService.validate({ foo: 1, blah: "test" }, null)
    ).to.throw("required schema argument is null or not an object");

    expect(() => validateService.validate(null, schema)).to.throw(
      "required json argument is null"
    );

    expect(() =>
      validateService.validate({ foo: 1, blah: "test" }, 1)
    ).to.throw("required schema argument is null or not an object");

    expect(() =>
      validateService.validate({ foo: 1, blah: "test" }, false)
    ).to.throw("required schema argument is null or not an object");

    expect(() =>
      validateService.validate({ foo: 1, blah: "test" }, { type: "any" })
    ).to.throw(
      "schema compilation failed: schema is invalid: data.type should be equal to one of the allowed values, data.type should be array, data.type should match some schema in anyOf"
    );
  });
});
