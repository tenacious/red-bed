const testhelper = require("../../../../__fixtures/test-helper").create();

describe(testhelper.testName(__filename, 4), function() {
  it("should filter under a range of conditions", function() {
    let filterService = require(`../../../../../lib/services/red-bed-filter/red-bed-filter-service.js`).create(
      testhelper.mockConfig(),
      testhelper.mockCommon()
    );
    expect(
      filterService.filter(["hello", "sifted", "array!"], {
        $in: ["hello", "world"]
      })
    ).to.eql(["hello"]);
    expect(
      filterService.filter([{ test: { data: 1 } }, { test: { data: 2 } }], {
        "test.data": { $eq: 2 }
      })
    ).to.eql([{ test: { data: 2 } }]);
    expect(
      filterService.filter([{ test: { data: 1 } }, { test: { data: 2 } }], {
        "test.data": { $eq: 3 }
      })
    ).to.eql([]);
    expect(filterService.filter(null, null)).to.eql([]);
    expect(filterService.filter(["hello", "sifted", "array!"], null)).to.eql(
      []
    );
  });
});
