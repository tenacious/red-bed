const { expect } = require("chai");
const testhelper = require("../../../../__fixtures/test-helper").create();
const svc = require(`../../../../../lib/services/red-bed-data/red-bed-data-service.js`);

describe(testhelper.testName(__filename, 4), function() {
  before(function() {
    this.svc = svc.create(
      testhelper.mockConfig(),
      testhelper.mockCommon(),
      null
    );
  });

  it("should insert some data", async function() {
    const currentDoc = await this.svc.set("testKey", "testValue");
    currentDoc.should.containEql({
      key: "testKey",
      value: "testValue"
    });
  });

  it("should retrieve some data", async function() {
    const currentDoc = await this.svc.get("testKey");
    currentDoc.should.containEql({
      key: "testKey",
      value: "testValue"
    });

    const invalidDoc = await this.svc.get("invalidKey");
    expect(invalidDoc).to.be.null;
  });

  it("should remove some data", async function() {
    const currentDoc = await this.svc.clear("testKey");
    expect(currentDoc).to.equal(1);

    const invalidDoc = await this.svc.get("testKey");
    expect(invalidDoc).to.be.null;
  });
});
