const helper = require("../../../../__fixtures/test-helper").create();
const StateService = require("../../../../../lib/services/red-bed-statemachine/red-bed-statemachine-service");

describe(helper.testName(__filename, 4), function() {
  this.timeout(20000);

  function mockConfig() {
    return {
      ENVIRONMENT_VARIABLES: {
        SHIP: "true"
      }
    };
  }

  function mockCommon() {
    let Utils = new require("red-bed").Common.Utils;
    let Logger = new require("red-bed").Common.Logger;
    return {
      utils: new Utils(),
      logger: new Logger()
    };
  }

  let stateServiceInstance = null;

  afterEach(function(done) {
    stateServiceInstance.machines = {
      GLOBAL: {
        STATE: "UNCHANGED"
      }
    };
    done();
  });

  it("creates service instance", async () => {
    stateServiceInstance = StateService.create(mockConfig(), mockCommon());
  });

  it("check machine SET/GET", async () => {
    stateServiceInstance.setState().should.equal("UNCHANGED");
    stateServiceInstance.getState().should.equal("UNCHANGED");
    stateServiceInstance.setState("CHANGED").should.equal("CHANGED");
    stateServiceInstance.getState().should.equal("CHANGED");

    stateServiceInstance
      .setState("new_VALUE", "new_STATE")
      .should.equal("new_VALUE");
    stateServiceInstance.getState("new_STATE").should.equal("new_VALUE");

    stateServiceInstance
      .setState("new_MACHINE_VALUE", "new_STATE", "new_MACHINE")
      .should.equal("new_MACHINE_VALUE");
    stateServiceInstance
      .getState("new_STATE", "new_MACHINE")
      .should.equal("new_MACHINE_VALUE");
  });

  it("check unknown machine GET", async () => {
    stateServiceInstance
      .getState(null, "UNKOWN_MACHINE")
      .should.equal("UNKNOWN");
    stateServiceInstance.getState("UNKNOWN_KEY").should.equal("UNKNOWN");
    stateServiceInstance
      .getState("UNKNOWN_KEY", "UNKOWN_MACHINE")
      .should.equal("UNKNOWN");
  });
});
