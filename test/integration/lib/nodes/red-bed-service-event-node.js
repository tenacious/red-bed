const helper = require("../../../__fixtures/test-helper").create();
const path = require("path");
const delay = require("await-delay");

describe(helper.testName(__filename, 4), function() {
  this.timeout(20000);

  afterEach(async function() {
    await this.server.stop();
    await delay(2000);
  });

  it("passing in config and pushing a message through", async function() {
    this.server = require("red-bed").Server.create({
      projectPath: path.resolve(
        __dirname,
        "../../../__fixtures/example-project"
      )
    });

    await this.server.start();
    await delay(2000);

    let response = await helper.testHttpPostRequest("/event/example1/test", {
      test: "payload",
      http: true
    });

    delete response.lastEvent.timestamp;
    expect(response).to.eql({
      test: "payload",
      http: true,
      lastEvent: {
        eventKey: "test-event"
      }
    });

    response = await helper.testHttpPostRequest("/event/all", {
      test: "payload",
      http: true
    });
    delete response.lastEvent.timestamp;
    expect(response).to.eql({
      test: "payload",
      http: true,
      lastEvent: {
        eventKey: "test-event-1"
      }
    });
  });
});
