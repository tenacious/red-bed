const helper = require("../../__fixtures/test-helper").create();
const path = require("path");
const delay = require("await-delay");
//const printOpenHandles = require('why-is-node-running');

describe(helper.testName(__filename, 4), function() {
  this.timeout(15000);

  afterEach(async function() {
    if (!this.server) return;

    await this.server.stop();
    await delay(2000);

    delete this.server;
  });

  it("tests initializing a server", async () => {
    require("red-bed").Server.create({
      projectPath: path.resolve(__dirname, "../../__fixtures/example-project")
    });
  });

  it("tests initializing and starting a server - passing in config and pushing a message through", async function() {
    let server = (this.server = require("red-bed").Server.create({
      projectPath: path.resolve(__dirname, "../../__fixtures/example-project"),
      nodered: {
        editorTheme: {
          page: {
            title: "Red-Bed Test Project"
            //favicon: "/absolute/path/to/theme/icon", //can use '__dirname + "\\img\\favicon.png" (\\ on Windows)'
            //css: "/absolute/path/to/custom/css/file",
            //scripts: "/absolute/path/to/custom/js/file" // As of 0.17
          },
          header: {
            title: "Red-Bed Test Project"
            //image: "/absolute/path/to/header/image", // or null to remove image
            //url: "http://nodered.org" // optional url to make the header text/image a link to this url
          },

          deployButton: {
            // type: "simple",
            // label: "Save",
            // icon: "/absolute/path/to/deploy/button/image" // or null to remove image
          },

          menu: {
            // Hide unwanted menu items by id. see editor/js/main.js:loadEditor for complete list
            // "menu-item-import-library": false,
            // "menu-item-export-library": false,
            // "menu-item-keyboard-shortcuts": false,
            // "menu-item-help": {
            //   label: "Alternative Help Link Text",
            //   url: "http://example.com"
            // }
          },

          userMenu: true, // Hide the user-menu even if adminAuth is enabled

          login: {
            //image: "/absolute/path/to/login/page/big/image" // a 256x256 image
          },

          logout: {
            //redirect: "http://example.com" // As of 0.17
          },

          palette: {
            editable: false // Enable/disable the Palette Manager
            // catalogues: [ // Alternative palette manager catalogues
            //   'https://catalogue.nodered.org/catalogue.json'
            // ],
            // theme: [ // Override node colours - rules test against category/type by RegExp.
            //   {
            //     category: ".*",
            //     type: ".*",
            //     color: "#f0f"
            //   }
            // ]
          },

          projects: {
            enabled: false // Enable the projects feature
          }
        }
      },
      nodes: {
        filter: {
          names: [
            "catch",
            "httpin",
            "example-node-1",
            "example-node-2",
            "switch",
            "ui-switch"
          ],
          type: "whitelist"
        }
      }
    }));

    await server.start();
    await delay(2000);

    // Test if auth config is loaded from config.js
    expect(server.config.nodered.adminAuth).to.not.be.undefined;
    expect(server.config.nodered.webServer).to.be.undefined;

    let response = await helper.testHttpGetRequest("/test");

    expect(response.example1).to.be.true;
    expect(response.example2).to.be.true;

    response = await helper.testHttpPostRequest("/test", {
      test: "payload"
    });

    expect(response.example1).to.be.true;
    expect(response.example2).to.be.true;
    expect(response.test).to.equal("payload");

    await expect(
      helper.testHttpPostRequest("/test", {
        test: "payload",
        fail: true
      })
    ).to.eventually.be.rejectedWith("failure happened");
  });

  it("tests initializing and starting a server, we ensure the example1 service has correct config and defaults", async function() {
    let server = (this.server = require("red-bed").Server.create({
      projectPath: path.resolve(__dirname, "../../__fixtures/example-project")
    }));

    await server.start();
    await delay(2000);

    expect(server.container.services.example1.config.defaultSetting).to.be.true;
    expect(
      server.container.services.example1.config.services.example1.customSetting1
    ).to.be.false;
    expect(
      server.container.services.example1.config.services.example1.customSetting2
    ).to.be.true;
  });

  it("tests initializing and starting a server - failure due to circular dependencies", () => {
    expect(
      (async () => {
        let server = require("red-bed").Server.create({
          projectPath: path.resolve(
            __dirname,
            "../../__fixtures/example-project-circular-services-dependencies"
          )
        });

        await server.start();
        await delay(5000);
        await server.stop();
        await delay(2000);
      })()
    ).to.be.rejectedWith(
      "Circular dependency detected: between example1 and example2 services detected, please check the service constructors"
    );
  });

  it("tests initializing and starting a server - failure due to circular dependency to self", () => {
    expect(
      (async () => {
        let server = require("red-bed").Server.create({
          projectPath: path.resolve(
            __dirname,
            "../../__fixtures/example-project-circular-services-dependencies-self"
          )
        });

        await server.start();
        await delay(5000);
        await server.stop();
        await delay(2000);
      })()
    ).to.be.rejectedWith(
      "Circular dependency detected: the example1 service cannot depend on itself, please check the service constructors"
    );
  });
});
