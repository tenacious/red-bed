var path = require("path");

function TestHelper() {
  this.request = require("request");
}

TestHelper.create = function() {
  return new TestHelper();
};

TestHelper.prototype.testName = function(testFilename, depth) {
  if (!depth) depth = 2;

  var fileParts = testFilename.split(path.sep).reverse();

  var poParts = [];

  for (var i = 0; i < depth; i++) poParts.push(fileParts.shift());

  return poParts
    .reverse()
    .join("/")
    .replace(".js", "");
};

TestHelper.prototype.mockConfig = function() {
  return {};
};

TestHelper.prototype.mockCommon = function() {
  let Utils = new require("red-bed").Common.Utils;
  let Logger = new require("red-bed").Common.Logger;
  return {
    utils: new Utils(),
    logger: new Logger()
  };
};

TestHelper.prototype.testHttpGetRequest = function(path) {
  if (path.indexOf("/") === 0) path = path.substring(1);
  return new Promise((resolve, reject) => {
    this.request(`http://127.0.0.1:8000/api/${path}`, function(
      error,
      response,
      body
    ) {
      if (error) return reject(new Error(error));
      resolve(JSON.parse(body));
    });
  });
};

TestHelper.prototype.testHttpPostRequest = function(path, payload) {
  if (path.indexOf("/") === 0) path = path.substring(1);
  return new Promise((resolve, reject) => {
    this.request(
      {
        url: `http://127.0.0.1:8000/api/${path}`,
        method: "POST",
        json: payload
      },
      function(error, response, body) {
        if (response.statusCode === 500)
          return reject(new Error("failure happened"));
        resolve(body);
      }
    );
  });
};

module.exports = TestHelper;
