const { use } = require("chai");

var chaiAsPromised = require("chai-as-promised");
use(chaiAsPromised);

require("chai/register-should");
require("chai/register-expect");

global.sinon = require("sinon");

const delay = require("await-delay");
global.helper = require("node-red-node-test-helper");
global.loadAsAsync = (nodeInstance, flow, errorDelay = 2000) =>
  new Promise((resolve, reject) =>
    helper.load(
      [nodeInstance, require("@node-red/nodes/core/common/25-catch.js")],
      [
        ...flow,
        {
          id: "id-rnd-rb-test-n3",
          type: "catch",
          wires: [["id-rnd-rb-test-n4"]]
        },
        {
          id: "id-rnd-rb-test-n4",
          type: "helper"
        }
      ],
      e => {
        if (e) return reject(e);
        delay(2000).then(() =>
          resolve(
            handler =>
              new Promise((resolve, reject) => {
                helper
                  .getNode(flow[flow.length - 1].id)
                  .on("input", function() {
                    delay(errorDelay).then(resolve);
                  });
                helper.getNode("id-rnd-rb-test-n4").on("input", function(msg) {
                  if (handler) {
                    try {
                      handler(msg);
                      resolve();
                    } catch (e) {
                      reject(e);
                    }
                  }
                  reject(msg.error);
                });
              })
          )
        );
      }
    )
  );
