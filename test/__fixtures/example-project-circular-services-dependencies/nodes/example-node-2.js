module.exports = function(RED) {
  let services = require("red-bed").Container.instance().services;
  services.example2.initNode();
  function DataNode(config) {
    RED.nodes.createNode(this, config);
    let node = this;
    node.on("input", function(msg, send, done) {
      msg.payload.example2 = true;
      send(msg);
      done();
    });
  }
  RED.nodes.registerType("example-node-2", DataNode);
};
