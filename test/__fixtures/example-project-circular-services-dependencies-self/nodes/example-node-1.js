module.exports = function(RED) {
  let services = require("red-bed").Container.instance().services;
  services.example1.initNode();
  function DataNode(config) {
    RED.nodes.createNode(this, config);
    let node = this;
    node.on("input", function(msg, send, done) {
      if (msg.payload.fail) {
        //eslint-disable-next-line
        console.log("failing good!");
        send(msg);
        done("test error happened");
      }
      msg.payload.example1 = true;
      send(msg);
    });
  }
  RED.nodes.registerType("example-node-1", DataNode);
};
