const Service = require("red-bed").Service;

class Example1Service extends Service {
  constructor(config, common, example1) {
    super(config, common);
    this.example1Service = example1;
  }

  async init() {}

  async start() {}

  async stop() {}

  defaults() {}

  async initNode() {
    this.common.logger.info("service1 init node");
  }

  async doSomething(msg) {
    let newMsg = this.common.utils.clone(msg);
    newMsg.example1DidSomething = true;
    return this.example2Service.doSomething(newMsg);
  }

  async doAnotherthing(msg) {
    msg.example1DidSomethingElse = true;
  }
}

module.exports = Example1Service;
