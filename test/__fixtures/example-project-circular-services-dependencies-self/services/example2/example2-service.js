const Service = require("red-bed").Service;

class Example2Service extends Service {
  constructor(config, common, example1) {
    super(config, common);
    this.example1Service = example1;
  }

  async init() {}

  async start() {}

  async stop() {}

  defaults() {}

  async initNode() {
    this.common.logger.info("service2 init node");
  }

  async doSomething(msg) {
    msg.example2DidSomething = true;
    return msg;
  }
}

module.exports = Example2Service;
