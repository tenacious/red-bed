module.exports = {
  serviceName: "red-bed-example-project",
  nodered: {
    adminAuth: {
      type: "credentials",
      users: [
        {
          username: "admin",
          password:
            "$2a$08$zZWtXTja0fB1pzD4sHCMyOCMYz2Z6dNbM6tl8sJogENOMcxWV9DN.",
          permissions: "*"
        }
      ]
    },
    editorTheme: {
      page: {
        title: "Red-Bed Test Project"
        //favicon: "/absolute/path/to/theme/icon", //can use '__dirname + "\\img\\favicon.png" (\\ on Windows)'
        //css: "/absolute/path/to/custom/css/file",
        //scripts: "/absolute/path/to/custom/js/file" // As of 0.17
      },
      header: {
        title: "Red-Bed Test Project"
        //image: "/absolute/path/to/header/image", // or null to remove image
        //url: "http://nodered.org" // optional url to make the header text/image a link to this url
      },

      deployButton: {
        // type: "simple",
        // label: "Save",
        // icon: "/absolute/path/to/deploy/button/image" // or null to remove image
      },

      menu: {
        // Hide unwanted menu items by id. see editor/js/main.js:loadEditor for complete list
        // "menu-item-import-library": false,
        // "menu-item-export-library": false,
        // "menu-item-keyboard-shortcuts": false,
        // "menu-item-help": {
        //   label: "Alternative Help Link Text",
        //   url: "http://example.com"
        // }
      },

      //userMenu: true, // Hide the user-menu even if adminAuth is enabled

      // login: {
      //   image: "/absolute/path/to/login/page/big/image" // a 256x256 image
      // },

      // logout: {
      //   //redirect: "http://example.com" // As of 0.17
      // },

      palette: {
        editable: true // Enable/disable the Palette Manager
        // catalogues: [ // Alternative palette manager catalogues
        //   'https://catalogue.nodered.org/catalogue.json'
        // ],
        // theme: [ // Override node colours - rules test against category/type by RegExp.
        //   {
        //     category: ".*",
        //     type: ".*",
        //     color: "#f0f"
        //   }
        // ]
      },

      projects: {
        enabled: false // Enable the projects feature
      }
    }
  },
  nodes: {
    // filter: {
    //   names: [
    //     "catch",
    //     "httpin",
    //     "example-node-1",
    //     "example-node-2",
    //     "switch",
    //     "ui-switch"
    //   ],
    //   type: "whitelist"
    // }
  },
  services: {
    example1: {
      //config settings for example
      customSetting1: false,
      customSetting2: true
    }
  }
};
