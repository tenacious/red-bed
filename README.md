[![pipeline status](https://gitlab.com/tenacious/red-bed/badges/master/pipeline.svg)](https://gitlab.com/tenacious/red-bed/commits/master) [![coverage report](https://gitlab.com/tenacious/red-bed/badges/master/coverage.svg)](https://gitlab.com/tenacious/red-bed/commits/master)

red-bed:
-------
*Embedded node-red with convention based layout and dependency injection*

premise:
--------
This module makes it really easy to create an embedded node-red project, it also consumes a convention-based folder structure that allows for the creation of a set of base services - with dependency injection baked in via the [node-dependency-injection](https://www.npmjs.com/package/node-dependency-injection) module.

The custom nodes for your embedded project are also created within the folder structure and are automatically built into the solution, they can access the base services. The idea here is that the nodes and node-red do flow control and expose endpoints and the services do the heavy lifting.

More about the folder structure:
--------------------------------
```
--project root
      |____node-red  --> the folder flows and node-red config lives in
      |____nodes     --> where custom nodes are set up
      |____public    --> static content
      |____schema    --> json schemas, for schema validation of node-red messages, not implemented yet
      |____services
      |       |____service name
      |                |____service name-service.js
      |____config.js --> the configuration file for the project, look here for [the example config](https://gitlab.com/tenacious/red-bed/tree/master/test/__fixtures/example-project/config.js)
```
dependency injection and services in more detail:
------------------------------------------------
NB: service class files must be named according to the convention: (project root)/services/(service name)/(service name)-service.js
*all services inherit from the red-bed Service class*
```javascript
const Service = require('red-bed').Service;

class Example1Service extends Service {
  //config -> the service config, actually the whole config for red-bed, so not just limited to config.services.example1
  //common -> injected common utilities and logging
  //example2 -> the injected example2 service, which is found by performing reflection on the constructor
  constructor(config, common, example2){
    super(config, common);
    this.example2Service = example2;
  }

  async init(){ //for initializing dependencies

  }

  async start(){ //the startup method

  }

  async stop(){ //the stop and teardown method

  }

  defaults(){ //setting up defaults
    this.config.customSetting = true;
  }
```

getting started with the example red-bed service:
------------------------------------------------

1. Clone, install and run, -p = project folder, -w = admin password:
```bash
git clone https://gitlab.com/tenacious/red-bed.git && cd red-bed && npm i && node bin/init -p test/__fixtures/example-project -w test
```
2. Login and view node-red setup:
you can login to node-red on http://localhost:8000/admin (username admin, password test) and modify/view the test flow

getting started with your own red-bed service:
----------------------------------------------

1. Install
----------
```bash
npm i red-bed
```

2. Folder structure and config file
-----------------------------------
copy the contents [test example folder structure](https://gitlab.com/tenacious/red-bed/tree/master/test/__fixtures/example-project) to your project folder path, or lib folder.

3. Create a runner js file
--------------------------
```javascript
let server = require('red-bed').Server.create({
  projectPath: path.resolve(__dirname, './my/project/folder')
});
await server.start();
//await server.stop(); //for later
```

4. Run runner.js
-----------------
```bash
node runner.js
```

5. You can view the test flows now, go to http://localhost:8000/admin, login with username admin, password can be anything for now.
--------------------------------------------------------------------------------------------------
