class StateMachineService extends require("red-bed").Service {
  constructor(config, common) {
    super(config, common);
    this.machines = {
      GLOBAL: {
        STATE: "UNCHANGED"
      }
    };
  }

  static create(config, common) {
    return new StateMachineService(config, common);
  }

  setState(stateValue, stateKey, machine) {
    if (stateValue == null) stateValue = "UNCHANGED";
    if (machine == null) machine = "GLOBAL";
    if (stateKey == null) stateKey = "STATE";
    if (!this.machines[machine]) this.machines[machine] = {};
    this.machines[machine][stateKey] = stateValue;
    return stateValue;
  }

  getState(stateKey, machine) {
    if (stateKey == null) stateKey = "STATE";
    if (machine == null) machine = "GLOBAL";
    if (!this.machines[machine]) return "UNKNOWN";
    if (this.machines[machine][stateKey] == null) return "UNKNOWN";
    return this.machines[machine][stateKey];
  }
}

module.exports = StateMachineService;
