const sift = require("sift").default;
class FilterService extends require("red-bed").Service {
  constructor(config, common) {
    super(config, common);
  }

  static create(config, common) {
    return new FilterService(config, common);
  }

  filter(input, filter) {
    if (input == null) return [];
    const inputArray = Array.isArray(input) ? input : [input];
    const found = inputArray.filter(sift(filter));
    return this.common.utils.clone(found);
  }
}

module.exports = FilterService;
