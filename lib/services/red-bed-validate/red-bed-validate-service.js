const Ajv = require("ajv");
const ajv = new Ajv({ schemaId: "auto" });
ajv.addMetaSchema(require("ajv/lib/refs/json-schema-draft-06.json"));
ajv.addMetaSchema(require("ajv/lib/refs/json-schema-draft-04.json"));

class ValidateService extends require("red-bed").Service {
  constructor(config, common) {
    super(config, common);
    this.compiledSchemas = {};
  }

  static create(config, common) {
    return new ValidateService(config, common);
  }

  validate(json, schema) {
    if (json == null) throw new Error("required json argument is null");
    if (schema == null || typeof schema !== "object")
      throw new Error("required schema argument is null or not an object");

    let schemaHash;

    try {
      schemaHash = this.common.utils.crypto.hash({
        type: "SHA1",
        value: schema
      });
      this.compiledSchemas[schemaHash] = ajv.compile(schema);
    } catch (e) {
      throw new Error("schema compilation failed: " + e.message);
    }

    let valid = this.compiledSchemas[schemaHash](json);
    return {
      valid,
      errors: this.compiledSchemas[schemaHash].errors
    };
  }
}

module.exports = ValidateService;
