const { Service } = require("red-bed");
const Datastore = require("nedb");

class DataService extends Service {
  constructor(config, common) {
    super(config, common);

    this.db = new Datastore({
      filename: __dirname + "/.store.db",
      autoload: true
    });
    this.db.ensureIndex({ fieldName: "key", unique: true });
  }

  static create(config, common) {
    return new DataService(config, common);
  }

  get(key) {
    return new Promise((resolve, reject) => {
      this.db.findOne({ key }, (err, doc) => {
        if (err) return reject(err);
        resolve(doc);
      });
    });
  }

  set(key, value) {
    const doc = {
      key,
      value
    };

    return new Promise((resolve, reject) => {
      this.get(key).then(existingDoc => {
        if (existingDoc) {
          this.db.update({ key }, doc, err => {
            if (err) return reject(err);
            this.get(key).then(newDoc => resolve(newDoc));
          });
        } else {
          this.db.insert(doc, (err, newDoc) => {
            if (err) return reject(err);
            resolve(newDoc);
          });
        }
      });
    });
  }

  clear(key) {
    return new Promise((resolve, reject) => {
      this.db.remove({ key }, {}, (err, numRemoved) => {
        if (err) return reject(err);
        resolve(numRemoved);
      });
    });
  }
}

module.exports = DataService;
