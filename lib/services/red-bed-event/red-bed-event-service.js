class EventService extends require("red-bed").Service {
  constructor(config, common) {
    super(config, common);
    this.__emit = this.emit;

    this.emit = (eventKey, payload) => {
      this.__emit("*", payload);
      this.__emit(eventKey, payload);
    };
  }

  static create(config, common) {
    return new EventService(config, common);
  }
}

module.exports = EventService;
