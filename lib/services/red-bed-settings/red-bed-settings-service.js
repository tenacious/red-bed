const Mustache = require("mustache"),
  traverse = require("traverse");

class SettingService extends require("red-bed").Service {
  constructor(config, common) {
    super(config, common);
    this.configurations = {};
  }

  static create(config, common) {
    return new SettingService(config, common);
  }

  injectSettingsVariables(obj) {
    let self = this;
    traverse(obj).forEach(function(val) {
      if (typeof val === "string" && val.indexOf("{{{SETTING://") === 0)
        this.update(self.injectSettingsVariable(val));
    });
    return obj;
  }

  injectSettingsVariable(template) {
    return Mustache.render(template.replace("SETTING://", ""), this.config);
  }
}

module.exports = SettingService;
