const RED = require("node-red");
module.exports = class RedWrapper {
  constructor(server) {
    this.server = server;
  }

  static create(server) {
    return new RedWrapper(server);
  }

  init(httpServer, config) {
    return RED.init(httpServer, config);
  }

  attach(app) {
    let self = this;

    app.use(self.server.config.nodered.httpAdminRoot, RED.httpAdmin);
    app.use(self.server.config.nodered.httpNodeRoot, RED.httpNode);

    RED.runtime.nodes.__getNodeList = RED.runtime.nodes.getNodeList;
    RED.runtime.nodes.__getNodeConfigs = RED.runtime.nodes.getNodeConfigs;

    //getEntries
    RED.runtime.nodes.getNodeList = opts => {
      return new Promise((resolve, reject) => {
        RED.runtime.nodes
          .__getNodeList(opts)
          .then(nodesList => {
            if (!self.server.config.nodes || !self.server.config.nodes.filter)
              return resolve(nodesList);
            return resolve(
              self.filterNodes(
                nodesList,
                self.server.config.nodes.filter.names,
                self.server.config.nodes.filter.type === "whitelist"
              )
            );
          })
          .catch(reject);
      });
    };

    RED.runtime.nodes.getNodeConfigs = opts => {
      return new Promise((resolve, reject) => {
        RED.runtime.nodes
          .__getNodeConfigs(opts)
          .then(configsScript => {
            if (!self.server.config.nodes || !self.server.config.nodes.filter)
              return resolve(configsScript);
            var configs = configsScript
              .trim()
              .split(/(?=<!-- --- \[red-module:\S+\] --- -->)/);
            return resolve(
              self.filterConfigs(
                configs,
                self.server.config.nodes.filter.names,
                self.server.config.nodes.filter.type === "whitelist"
              )
            );
          })
          .catch(reject);
      });
    };
  }

  filterConfigs(configsList, filter, whitelist) {
    return configsList
      .filter(config => {
        for (let filterName of filter) {
          if (
            whitelist &&
            config.indexOf(
              `<!-- --- [red-module:node-red-node-${filterName}/${filterName}] --- -->`
            ) === 0
          )
            return true;
          if (
            whitelist &&
            config.indexOf(
              `<!-- --- [red-module:node-red/${filterName}] --- -->`
            ) === 0
          )
            return true;
          if (
            !whitelist &&
            config.indexOf(
              `<!-- --- [red-module:node-red-node-${filterName}/${filterName}] --- -->`
            ) === -1
          )
            return true;
          if (
            !whitelist &&
            config.indexOf(
              `<!-- --- [red-module:node-red/${filterName}] --- -->`
            ) === -1
          )
            return true;
        }
        return !whitelist;
      })
      .join("");
  }

  filterNodes(nodesList, filter, whitelist) {
    this.server.common.logger.info(
      `node filter configured via ${whitelist ? "white" : "black"}list`
    );
    return nodesList.filter(node => {
      let allowed = whitelist
        ? filter.indexOf(node.name) > -1
        : filter.indexOf(node.name) === -1;
      if (!allowed)
        this.server.common.logger.warn(
          `node ${node.name} not allowed by configuration`
        );
      else
        this.server.common.logger.info(
          `node ${node.name} allowed by configuration`
        );
      return allowed;
    });
  }

  async start() {
    await RED.start();
  }

  async stop() {
    await RED.stop();
  }
};
