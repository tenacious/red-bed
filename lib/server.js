const http = require("http");
const express = require("express");
const path = require("path");
const _ = require("lodash");
const plebs = require("plebs");
const convenient = require("convenient-di");
const sillyname = require("happn-sillyname");
const fs = require("fs");

module.exports = class RedBed {
  constructor(config) {
    this.common = {};
    this.common.utils = plebs.Utils.create();
    this.common.logger = plebs.Logger.create(config.logger);
    this.common.crypto = plebs.Crypto.create(config.crypto);
    this.config = this.defaults(config);
    this.nodered = require("./red-wrapper").create(this);
  }

  static create(config) {
    return new RedBed(config);
  }

  saveExistingConfig(config) {
    const homedir = require("os").homedir();
    this.common.utils.fileWriteObject(
      homedir + path.sep + config.serviceName + ".json",
      {
        serviceName: config.serviceName,
        adminPassword: config.adminPassword,
        adminUsername: config.adminUsername
      }
    );
  }

  getExistingConfig(config) {
    const homedir = require("os").homedir();
    let data = this.common.utils.fileReadData(
      homedir + path.sep + config.serviceName + ".json"
    );
    if (data) return JSON.parse(data);
    return {};
  }

  removeExistingConfig(config) {
    const homedir = require("os").homedir();
    this.common.utils.fileDelete(
      homedir + path.sep + config.serviceName + ".json"
    );
  }

  async init() {
    this.container = convenient.Container.create(
      {
        projectPath: this.config.projectPath,
        services: this.config.services
      },
      this.common
    );

    this.container.register();
    this.container.resolve();

    await this.container.init();

    this.initialized = true;
  }

  async start() {
    //backward compatibility
    if (!this.initialized) await this.init();
    await this.container.start();
    this.app = express();

    let publicFolder = path.resolve(this.config.projectPath, "./public");
    if (this.common.utils.folderExists(publicFolder))
      this.app.use("/", express.static(publicFolder));

    let deaf = false;
    if (!this.webServer) {
      this.webServer = http.createServer(this.app);
      deaf = true;
    }

    this.nodered.init(this.webServer, this.config.nodered);
    this.nodered.attach(this.app);

    process.on(
      "SIGINT",
      (this._sigintHandler = () => {
        //eslint-disable-next-line
        this.common.logger.info("Caught interrupt signal; cleaning up server");
        this.stop().then(() => process.exit());
      })
    );

    if (deaf) this.webServer.listen(8000);
    await this.nodered.start();
  }

  async stop() {
    process.removeListener("SIGINT", this._sigintHandler);

    await this.container.stop();
    await this.webServer.close();
    await this.nodered.stop();

    if (!this._hasServiceNameBeenPassed) this.removeExistingConfig(this.config);
  }

  copyRedBedNodesToProjectFolder(projectPath) {
    const fromFolder = path.resolve(__dirname, "./nodes");
    const toFolder = path.resolve(projectPath, "./nodes");
    this.common.utils.getFiles(fromFolder).forEach(file => {
      fs.copyFileSync(
        `${fromFolder}${path.sep}${file.name}`,
        `${toFolder}${path.sep}${file.name}`
      );
    });

    const fromIconFolder = path.resolve(__dirname, "./nodes/icons");
    const toIconFolder = path.resolve(projectPath, "./nodes/icons");
    this.common.utils.getFiles(fromIconFolder).forEach(file => {
      if (file.name.indexOf(".png") >= 0)
        fs.copyFileSync(
          `${fromIconFolder}${path.sep}${file.name}`,
          `${toIconFolder}${path.sep}${file.name}`
        );
    });
  }

  copyRedBedServicesToProjectFolder(projectPath) {
    const fromFolder = path.resolve(__dirname, "./services");
    const toFolder = path.resolve(projectPath, "./services");

    this.common.utils.folderEnsure(`${toFolder}${path.sep}red-bed-settings`);
    fs.copyFileSync(
      `${fromFolder}${path.sep}red-bed-settings${path.sep}red-bed-settings-service.js`,
      `${toFolder}${path.sep}red-bed-settings${path.sep}red-bed-settings-service.js`
    );

    this.common.utils.folderEnsure(
      `${toFolder}${path.sep}red-bed-statemachine`
    );
    fs.copyFileSync(
      `${fromFolder}${path.sep}red-bed-statemachine${path.sep}red-bed-statemachine-service.js`,
      `${toFolder}${path.sep}red-bed-statemachine${path.sep}red-bed-statemachine-service.js`
    );

    this.common.utils.folderEnsure(`${toFolder}${path.sep}red-bed-event`);
    fs.copyFileSync(
      `${fromFolder}${path.sep}red-bed-event${path.sep}red-bed-event-service.js`,
      `${toFolder}${path.sep}red-bed-event${path.sep}red-bed-event-service.js`
    );

    this.common.utils.folderEnsure(`${toFolder}${path.sep}red-bed-filter`);
    fs.copyFileSync(
      `${fromFolder}${path.sep}red-bed-filter${path.sep}red-bed-filter-service.js`,
      `${toFolder}${path.sep}red-bed-filter${path.sep}red-bed-filter-service.js`
    );

    this.common.utils.folderEnsure(`${toFolder}${path.sep}red-bed-validate`);
    fs.copyFileSync(
      `${fromFolder}${path.sep}red-bed-validate${path.sep}red-bed-validate-service.js`,
      `${toFolder}${path.sep}red-bed-validate${path.sep}red-bed-validate-service.js`
    );

    this.common.utils.folderEnsure(`${toFolder}${path.sep}red-bed-data`);
    fs.copyFileSync(
      `${fromFolder}${path.sep}red-bed-data${path.sep}red-bed-data-service.js`,
      `${toFolder}${path.sep}red-bed-data${path.sep}red-bed-data-service.js`
    );
  }

  copyRedBedPublicToProjectFolder(projectPath) {
    const fromFolder = path.resolve(__dirname, "./public");
    const toFolder = path.resolve(projectPath, "./public");
    this.common.utils.folderEnsure(toFolder);
    this.common.utils.getFiles(fromFolder).forEach(file => {
      fs.copyFileSync(
        `${fromFolder}${path.sep}${file.name}`,
        `${toFolder}${path.sep}${file.name}`
      );
    });
  }

  defaults(config) {
    if (!config || !config.projectPath)
      throw new Error("Invalid config: projectPath not defined");

    if (!this.common.utils.folderExists(config.projectPath))
      throw new Error(
        `Invalid config: project folder on path ${config.projectPath} does not exist`
      );

    let clonedConfig = this.common.utils.clone(config);
    const existingConfig = this.getExistingConfig(clonedConfig);

    const projectConfigPath = path.resolve(
      clonedConfig.projectPath,
      "./config.js"
    );

    if (this.common.utils.fileExists(projectConfigPath)) {
      try {
        clonedConfig = _.defaultsDeep(clonedConfig, require(projectConfigPath));
      } catch (e) {
        throw new Error(
          `issue setting project defaults with config: ${projectConfigPath}: ${e.message}`
        );
      }
    }

    this._hasServiceNameBeenPassed = !!clonedConfig.serviceName;
    clonedConfig.serviceName = clonedConfig.serviceName || sillyname();
    this.common.logger.info(
      `starting service name: ${clonedConfig.serviceName}`
    );

    if (!clonedConfig.nodered) clonedConfig.nodered = {};

    this.adminAuth = clonedConfig.nodered.adminAuth;
    this.webServer = clonedConfig.nodered.webServer;

    if (!clonedConfig.nodered.flowFile)
      clonedConfig.nodered.flowFile = "red-bed-flows.json";
    //eslint-disable-next-line
    if (clonedConfig.nodered.disableEditor == null)
      clonedConfig.nodered.disableEditor = false;
    //eslint-disable-next-line
    if (clonedConfig.nodered.https == null) clonedConfig.nodered.https = false;
    //eslint-disable-next-line
    if (clonedConfig.nodered.uiHost == null)
      clonedConfig.nodered.uiHost = "0.0.0.0";
    //eslint-disable-next-line
    if (clonedConfig.nodered.httpAdminRoot == null)
      clonedConfig.nodered.httpAdminRoot = "/admin";
    //eslint-disable-next-line
    if (clonedConfig.nodered.httpNodeRoot == null)
      clonedConfig.nodered.httpNodeRoot = "/api";

    clonedConfig.nodered.userDir = path.resolve(
      clonedConfig.projectPath,
      "./node-red"
    );
    clonedConfig.nodered.nodesDir = path.resolve(
      clonedConfig.projectPath,
      "./nodes"
    );

    this.common.utils.folderEnsure(clonedConfig.nodered.userDir);
    this.common.utils.folderEnsure(clonedConfig.nodered.nodesDir);
    this.common.utils.folderEnsure(
      `${clonedConfig.nodered.nodesDir}${path.sep}icons`
    );

    this.copyRedBedNodesToProjectFolder(clonedConfig.projectPath);
    this.copyRedBedServicesToProjectFolder(clonedConfig.projectPath);
    this.copyRedBedPublicToProjectFolder(clonedConfig.projectPath);
    //eslint-disable-next-line
    if (this.adminAuth == null) {
      this.common.logger.warn("using file-based admin auth");
      clonedConfig.nodered.adminAuth = require("./security/file-auth");
      let adminPassword = existingConfig.adminPassword;
      let saltRounds = clonedConfig.saltRounds || 10;

      if (process.env.ADMIN_PWD || clonedConfig.adminPassword)
        //override with ENV or explicit config
        adminPassword = clonedConfig.nodered.adminAuth.getHash(
          process.env.ADMIN_PWD || clonedConfig.adminPassword,
          saltRounds
        );

      let defaultHash = clonedConfig.nodered.adminAuth.getHash(
        "admin",
        saltRounds
      );

      if (!adminPassword || defaultHash === adminPassword) {
        this.common.logger.warn(
          `no admin password for service name: ${clonedConfig.serviceName}, default of 'admin' is going to be used, not for production use!`
        );
        adminPassword = defaultHash;
      }
      clonedConfig.adminPassword = adminPassword;
      clonedConfig.adminUsername =
        clonedConfig.adminUsername || existingConfig.adminUsername || "admin";
      global.credentials = {
        adminUsername: clonedConfig.adminUsername,
        adminPassword: clonedConfig.adminPassword
      };
    } else clonedConfig.nodered.adminAuth = this.adminAuth;

    if (!clonedConfig.nodered.functionGlobalContext)
      clonedConfig.nodered.functionGlobalContext = {}; //empty context

    if (!clonedConfig.services) clonedConfig.services = {};
    this.saveExistingConfig(clonedConfig);
    return clonedConfig;
  }
};
