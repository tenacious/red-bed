const bcrypt = require("bcrypt");

function getHash(password, rounds) {
  var salt = bcrypt.genSaltSync(rounds);
  return bcrypt.hashSync(password, salt);
}

function compareHash(password, hash) {
  return bcrypt.compareSync(password, hash);
}

module.exports = {
  type: "credentials",
  getHash,
  compareHash,
  users: function() {
    return new Promise(resolve => {
      var valid = true;
      // Do whatever work is needed to check username is a valid
      // user.
      if (valid) {
        // Resolve with the user object. It must contain
        // properties 'username' and 'permissions'
        var user = {
          username: global.credentials.adminUsername,
          permissions: "*"
        };
        resolve(user);
      } else {
        // Resolve with null to indicate this user does not exist
        resolve(null);
      }
    });
  },
  authenticate: function(username, password) {
    return new Promise(resolve => {
      if (
        username === global.credentials.adminUsername &&
        compareHash(password, global.credentials.adminPassword)
      ) {
        // Resolve with the user object. Equivalent to having
        // called users(username);
        var user = {
          username,
          permissions: "*"
        };
        resolve(user);
      } else {
        // Resolve with null to indicate the username/password pair
        // were not valid.
        resolve(null);
      }
    });
  },
  default: function() {
    return new Promise(function(resolve) {
      // Resolve with the user object for the default user.
      // If no default user exists, resolve with null.
      resolve({
        anonymous: true,
        permissions: "read"
      });
    });
  }
};
