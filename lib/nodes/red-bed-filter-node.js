module.exports = function(RED) {
  const _ = require("underscore");
  const filterService = require("red-bed").Container.instance().services[
    "red-bed-filter"
  ];

  function FilterNode(config) {
    RED.nodes.createNode(this, config);
    const node = this;

    node.on("input", async (msg, send, done) => {
      try {
        if (!config.filterPayloadPath)
          throw new Error("filterPayloadPath not configured");
        if (!config.filter) throw new Error("filter not configured");

        let parsedFilter;
        if (typeof config.filter === "string")
          try {
            parsedFilter = JSON.parse(config.filter);
          } catch (e) {
            throw new Error("bad filter JSON: " + e.message);
          }
        else parsedFilter = config.filter;

        const filterPayload = _.propertyOf({ msg })(
          config.filterPayloadPath.split(".")
        );
        msg.payload.filtered = filterService.filter(
          filterPayload,
          parsedFilter
        );
        send(msg);
        done();
      } catch (e) {
        this.emit("node-error", e);
        done(e);
      }
    });
  }
  RED.nodes.registerType("filter-rb", FilterNode);
};
