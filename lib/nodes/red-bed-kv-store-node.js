const _ = require("underscore");
const { Container } = require("red-bed");

class StoreNode {
  constructor(config) {
    this.config = config;
    this.dataService = Container.instance().services["red-bed-data"];

    this.handleInput = this.handleInput.bind(this);
  }

  async get(msg, key) {
    msg.payload.kv_result = await this.dataService.get(key);
  }

  async set(msg, key) {
    if (!this.config.valuePayloadPath)
      throw new Error("valuePayloadPath not configured");

    const valuePayload = _.propertyOf({ msg })(
      this.config.valuePayloadPath.split(".")
    );

    msg.payload.kv_result = await this.dataService.set(key, valuePayload);
  }

  async unset(msg, key) {
    msg.payload.kv_result = await this.dataService.clear(key);
  }

  async handleInput(msg) {
    if (!["GET", "SET", "UNSET"].includes(this.config.kvOperation))
      throw new Error("Invalid operation; either GET or SET must be selected");

    await this[this.config.kvOperation.toLowerCase()](msg, this.config.key);
  }
}

module.exports = function(RED) {
  function KvStoreNode(config) {
    const storeInstance = new StoreNode(config);

    RED.nodes.createNode(this, config);
    this.on("input", async (msg, send, done) => {
      try {
        await storeInstance.handleInput(msg);
        send(msg);
        done();
      } catch (e) {
        this.emit("node-error", e);
        done(e);
      }
    });
  }
  RED.nodes.registerType("key-value-rb", KvStoreNode);
};
