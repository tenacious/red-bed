module.exports = function(RED) {
  let stateService = require("red-bed").Container.instance().services[
    "red-bed-statemachine"
  ];

  function DataNode(config) {
    RED.nodes.createNode(this, config);
    let node = this;

    node.on("input", async (msg, send, done) => {
      try {
        if (!msg.redbed) msg.redbed = {};

        switch (config.stateOperation) {
          case "GET":
            msg.redbed.lastState = stateService.getState(
              config.stateKey,
              config.machine
            );
            break;

          case "SET":
            msg.redbed.lastState = stateService.setState(
              config.stateValue,
              config.stateKey,
              config.machine
            );
            break;

          case "EXPECT":
            msg.redbed.lastState = stateService.getState(
              config.stateKey,
              config.machine
            );
            //dont pass message on if expected state does not match, the ! is used for a not equals
            if (config.stateValue.indexOf("!") === 0) {
              //eslint-disable-next-line
              if (msg.redbed.lastState == config.stateValue.substring(1)) return;
            } else {
              //eslint-disable-next-line
              if (msg.redbed.lastState != config.stateValue) return;
            }
            break;

          default:
            throw new Error(
              `unexpected state operation: ${config.stateOperation}`
            );
        }
        send(msg);
        done();
      } catch (e) {
        this.emit("node-error", e);
        done(e);
      }
    });
  }
  RED.nodes.registerType("state-rb", DataNode);
};
