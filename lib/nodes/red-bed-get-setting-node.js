module.exports = function(RED) {
  let settingsService = require("red-bed").Container.instance().services
    .settings;

  function GetSettingNode(config) {
    RED.nodes.createNode(this, config);
    let node = this;

    node.on("input", async (msg, send, done) => {
      try {
        const variablePath = config.variablePath || "";
        msg.payload.lastSettingValue = settingsService.injectSettingsVariable(
          variablePath
        );
        send(msg);
        done();
      } catch (e) {
        this.emit("node-error", e);
        done(e);
      }
    });
  }
  RED.nodes.registerType("get-setting-rb", GetSettingNode);
};
