module.exports = function(RED) {
  const _ = require("underscore");
  let services = require("red-bed").Container.instance().services;

  function ServiceEventNode(config) {
    RED.nodes.createNode(this, config);

    let node = this;
    let service = services[config.service];

    if (!service) {
      let error = new Error(
        `service name ${config.service || "unknown"} not in services`
      );
      this.emit("node-error", error);
      node.error(error);
    }

    if (config.eventOperation === "ON") {
      node.listener = payload => {
        node.send({ payload });
      };
      service.on(config.eventKey, node.listener);
      node.on("close", () => {
        service.off(config.eventKey, node.listener);
      });
      return;
    }

    node.on("input", async (msg, send, done) => {
      try {
        if (!config.eventPayloadPath)
          throw new Error("eventPayloadPath not configured");
        if (!config.eventKey) throw new Error("eventKey not configured");

        let eventPayload = _.propertyOf({ msg })(
          config.eventPayloadPath.split(".")
        );
        service.emit(config.eventKey, eventPayload);
        msg.payload.lastEvent = {
          eventKey: config.eventKey,
          timestamp: Date.now()
        };
        send(msg);
        done();
      } catch (e) {
        this.emit("node-error", e);
        done(e);
      }
    });
  }
  RED.nodes.registerType("service-event-rb", ServiceEventNode, {
    settings: {
      serviceEventRbServiceNames: {
        value: Object.keys(services),
        exportable: true
      }
    }
  });
};
