module.exports = function(RED) {
  const _ = require("underscore");
  const validateService = require("red-bed").Container.instance().services[
    "red-bed-validate"
  ];

  function ValidateNode(config) {
    RED.nodes.createNode(this, config);
    const node = this;

    node.on("input", async (msg, send, done) => {
      try {
        if (!config.validatePayloadPath)
          throw new Error("validatePayloadPath not configured");
        if (!config.validate) throw new Error("validate not configured");

        let parsedValidate;
        if (typeof config.validate === "string")
          try {
            parsedValidate = JSON.parse(config.validate);
          } catch (e) {
            throw new Error("bad validate JSON: " + e.message);
          }
        else parsedValidate = config.validate;

        let validatePayload = _.propertyOf({ msg })(
          config.validatePayloadPath.split(".")
        );

        if (validatePayload == null)
          throw new Error(
            `unable to find validation payload on path: ${config.validatePayloadPath}`
          );

        msg.payload.validated = validateService.validate(
          validatePayload,
          parsedValidate
        );
        send(msg);
        done();
      } catch (e) {
        this.emit("node-error", e);
        done(e);
      }
    });
  }
  RED.nodes.registerType("validate-rb", ValidateNode);
};
