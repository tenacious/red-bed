//eslint-disable-next-line
function displayControls(elements, display) {
  elements.forEach(function(elementId) {
    getControl(elementId).style.display = display;
  });
}

function getControl(elementId) {
  return document.getElementById(elementId);
}
