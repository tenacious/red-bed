var program = require("commander");
var path = require("path");
var executionPath = process.cwd();

program
  .version(require("../package.json").version)
  .option("-p, --project-path [path]", "Project path")
  .option("-n, --service-name [name]", "Service name")
  .option("-w, --admin-password [password]", "Admin password")
  .option("-u, --admin-username [username]", "Admin username")
  .parse(process.argv);

if (!program.projectPath) throw new Error("no project path configured!");
let projectPath = program.projectPath;
if (projectPath.indexOf("./") === 0)
  projectPath = path.resolve(executionPath, projectPath);

let createConfig = {
  projectPath,
  adminUsername: program.adminUsername || "admin"
};

if (program.adminPassword) createConfig.adminPassword = program.adminPassword;
if (program.adminUsername) createConfig.adminUsername = program.adminUsername;
if (program.serviceName) createConfig.serviceName = program.serviceName;

require("..")
  .Server.create(createConfig)
  .start();
